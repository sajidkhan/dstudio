# Text-Classification-Docker-Webapp
First of all create python environment or conda environment using following command and then activate the environemnt</br>

                 conda create -n webapp python==3.7
                 conda activate webapp
                 
Run following command to install all necessary packages </br>

                 pip install -r requirements.txt
                 
Run following command to run app </br>

                 python app.py


## push to github

git add .
git commit -m 'a message ...'
git push

## from drplet
ssh root@139.59.24.210
cd Text-Classifier/
git pull
docker-compose up -d --build