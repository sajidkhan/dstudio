FROM python:3.7

WORKDIR /app

ADD . .

RUN pip install -r requirements.txt --no-cache-dir

EXPOSE 5000

ENV FLASK_APP main.py

ENV FLASK_DEBUG=1

CMD flask run --host=0.0.0.0
# ENTRYPOINT [ "flask run"]